
class OfficesList {

  List<Office> offices;

  OfficesList({this.offices});

  factory OfficesList.fromJson(Map<String, dynamic> json){
    var officesJson = json['offices'] as List;

    List<Office> officesList = officesJson.map((office) =>
        Office.fromJson(office)).toList();

    return OfficesList(
      offices: officesList
    );
  }
}


class Office {

  String name;
  String address;
  String image;

  Office({this.name, this.address, this.image});

  factory Office.fromJson(Map<String, dynamic> json){
    return Office(
      name: json['name'],
      address: json['address'],
      image: json['image']
    );
  }

}