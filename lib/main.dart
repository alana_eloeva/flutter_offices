import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:offices/offices.dart';

void main() {
  runApp(MaterialApp(
    home: HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  void initState() {
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Offices'),
      ),
      body: FutureBuilder<OfficesList>(
        future: _loadData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.offices.length,
                itemBuilder: (context, index){
                Office office = snapshot.data.offices[index];
                return Card(
                  child: ListTile(
                    title: Text(office.name),
                    subtitle: Text(office.address),
                    leading: Image.network(office.image),
                  ),
                );

                });
          }else if(snapshot.hasError){
            return Center(
              child: Text(snapshot.error)
            );
          }
          return Center(child: CircularProgressIndicator());
        },

      ),
    );
  }


  Future<OfficesList> _loadData() async {
    const url = 'https://about.google/static/data/locations.json';
    try {
      var response = await http.get(url);
      if (response.statusCode == 200) {
        return OfficesList.fromJson(json.decode(response.body));
      } else {
        print(response.reasonPhrase);
      }
    } catch (error) {
      print(error);
    }
  }

}
